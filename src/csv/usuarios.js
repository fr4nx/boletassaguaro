const Usuarios = {
  usuarios: [
    {
      nombre: 'Walter Manuel Vizarreta Vilcarromero',
      dpto: 202,
      correo: 'waltervizarreta@hotmail.com',
      telefono: '997-920445'
    },
    {
      nombre: 'Walter Víctor Mayme Cuya',
      dpto: 203,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Víctor Raul Cuadros Caselli',
      dpto: 204,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Jose Luis Arauco M.',
      dpto: 301,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Federico Valdivia Pantigoso',
      dpto: 302,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Jorge Hermilio Herrera Quispe',
      dpto: 303,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Marcelo Gonzáles',
      dpto: 401,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Federico Valdivia Pantigoso',
      dpto: 402,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Héctor Alonso Bueno Lujan',
      dpto: 403,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Cristina Toledo',
      dpto: 404,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Raul Mendoza Llihua',
      dpto: 502,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Cecilia Roxana Bazan Lino',
      dpto: 503,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Laura Meza Fernandez',
      dpto: 601,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Liliana Katherine Gamarra Romero',
      dpto: 602,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Oscar Alfredo Valdez Herrera',
      dpto: 603,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Gerardo Mechan Velasquez',
      dpto: 604,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maria Julia del Pilar Oyague Baertl',
      dpto: 701,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maria Ines Oyague Baertl',
      dpto: 702,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Manuel Coca',
      dpto: 703,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Carlos Albero Oyague Baertl',
      dpto: 801,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maria Amalia Oyague Baertl',
      dpto: 802,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Edith Palomino Padilla',
      dpto: 803,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maritza Augusta Monica Santolalla Gracey',
      dpto: 804,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maria Amalia Oyague Baertl',
      dpto: 901,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Sara Maria Oyague Baertl',
      dpto: 902,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Yesenia Aguilar Lugo',
      dpto: 903,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Duber Javier Perez Pereyra',
      dpto: 1001,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Elsa Aleja Manrique Torres de Bengoa',
      dpto: 1002,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Adelina de Castellano',
      dpto: 1003,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Lucia Del Pilar Villanueva Ortega',
      dpto: 1004,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Jenmer Peches Hernandez/ Rosa Choque',
      dpto: 1101,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Reinel Vasquez Del Aguila',
      dpto: 1102,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Maria Ines Oyague Baertl',
      dpto: 1103,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Katty Elisabeth Maguiña Aguedo',
      dpto: 1202,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Miguel Alonso Rojas Huillca',
      dpto: 1203,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Miguel Angel Leon Curazzi/Luis Miguel Leon Curazzi',
      dpto: 1204,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Nestor Vergani',
      dpto: 1301,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Luis Javier Guerra Cáceres',
      dpto: 1302,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'William Nestor Montes Tovar',
      dpto: 1303,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Nestor Vergani',
      dpto: 1401,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Carmen Huayllara Arica',
      dpto: 1402,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Ana Maria Barreda Arellano',
      dpto: 1403,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Juan Carlos Arenas Quispe',
      dpto: 1404,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Pedro Bergelund',
      dpto: 1501,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Juan Angelo Velarde Saravia',
      dpto: 1502,
      correo: '',
      telefono: ''
    },
    {
      nombre: 'Arturo Jordan Paulet Espinoza',
      dpto: 1504,
      correo: '',
      telefono: ''
    }
  ]
}
export { Usuarios }
