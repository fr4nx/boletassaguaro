const Cocheras = {
  cocheras: [
    {
      nombre: 'Banco de Crédito del Perú',
      dpto: 201,
      correo: '',
      telefono: '',
      park: [30, 35]
    },
    {
      nombre: 'Banco de Crédito del Perú',
      dpto: 501,
      correo: '',
      telefono: '',
      park: [36, 39]
    },
    {
      nombre: 'Banco de Crédito del Perú',
      dpto: 1201,
      correo: '',
      telefono: '',
      park: [42, 43]
    }
  ]
}

export { Cocheras }
