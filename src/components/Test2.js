import React, { Component } from 'react'
// import { Usuarios } from '../csv/usuarios'
import { Cocheras } from '../csv/cocheras'

// const boleta = (mes) => (

// )

export default class TestComponent extends Component {
  render () {
    const { cocheras } = Cocheras
    // const fechaVencimiento = '22/10/2019'
    // const fechaHoy = '15 de Octubre de 2019'
    const meses = ['DICIEMBRE'] //, 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SETIEMBRE', 'OCTUBRE']
    // const concep = 'Mantenimiento por 19 días'
    const ultimoBoleta = 413 // 317 ultimo en setiembre
    let boleta = 0
    return (
      <center>
        {meses.map((mes, i) =>
          cocheras.map((cochera, j) => (
            <div
              style={{
                // marginTop: boleta % 2 ? '0px' : '30px',
                paddingTop: 39,
                display: 'block'
              }}
              key={i}
            >
              <table border={0} width={900}>
                <tr>
                  <td colSpan={1} width={100}>
                    <img
                      src={
                        'https://img10.naventcdn.com/avisos/11/00/50/61/26/14/1200x1200/28857083.jpg'
                      }
                      style={{
                        width: '100%',
                        height: 'auto',
                        filter: 'grayscale(100%)'
                      }}
                    />
                  </td>
                  <td colSpan={2}>
                    <table
                      border={0}
                      width={500}
                      style={{ textAlign: 'center' }}
                    >
                      <tr>
                        <td>
                          <h4 style={{ margin: 0 }}>JUNTA DE PROPIETARIOS</h4>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <h4 style={{ margin: 0 }}>"EDIFICIO SAGUARO"</h4>
                        </td>
                      </tr>
                      <tr>
                        {/* <td>
                            <h4 style={{ margin: 5 }}>DIRECCIÓN:</h4>
                          </td> */}
                        <td>
                          <h5 style={{ margin: 0 }}>
                            AV. PARDO DE ZELA 570 - LINCE
                          </h5>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td colSpan={1}>
                    <table width={300} style={{ border: '2px solid black' }}>
                      <tr>
                        <td style={{ textAlign: 'center' }}>
                          Recibo de Mantenimiento de Áreas Comunes
                        </td>
                      </tr>
                      <tr>
                        <td style={{ textAlign: 'center' }}>
                          <b>
                            Nro. {('000' + (boleta++ + ultimoBoleta)).slice(-4)}
                          </b>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {/* <tr>
                    <td />
                    <td />
                    <td />
                    <td style={{ textAlign: 'center' }}>
                      <h3 style={{ margin: 0 }}>{`${mes} ${i ? 2019 : 2018}`}</h3>
                    </td>
                  </tr>

                  <tr>
                    <td colSpan={3}>
                      <h4 style={{ margin: 0 }}>Lima, {fechaHoy}</h4>
                    </td>
                  </tr> */}
              </table>
              <table border={0} width={900}>
                <tr>
                  <td>
                    <table border={0} width={500}>
                      <tr>
                        <td>Recibimos de:</td>
                        <td colSpan={3} style={{ textTransform: 'uppercase' }}>
                          <b>{cochera.nombre}</b>
                        </td>
                      </tr>
                      {/* <tr>
                    <td>La cantidad de:</td>
                    <td colSpan={3}>{i ? 'TRECIENTOS TREINTA Y 00/100' : 'DOSCIENTOS DOS Y 27/100'} NUEVOS SOLES</td>
                  </tr> */}
                      <tr>
                        <td>Correspondiente al mes de:</td>
                        <td>{`${mes} ${i ? 2019 : 2018}`}</td>
                      </tr>
                      {/* <tr>
                    <td>Año</td>
                    <td></td>
                  </tr> */}
                      <tr>
                        <td>Departamento</td>
                        <td>{cochera.dpto}</td>
                      </tr>
                      <tr>
                        <td>Fecha de depósito</td>
                        <td>___________________</td>
                      </tr>
                    </table>
                  </td>
                  <td>
                    <div
                      style={{
                        textAlign: 'center',
                        border: '2px solid gray',
                        padding: '5px',
                        margin: '5px 0',
                        fontSize: '12px'
                      }}
                    >
                      <p style={{ marginBottom: '5px' }}>
                        Exonerado del pago de impuesto según D.L. 19620
                      </p>
                      <p style={{ marginBottom: '5px' }}>
                        Según acuerdo de la Junta de Propietarios, los recibos
                        deben pagarse dentro de los 10 primeros días del mes
                        correspondiente.
                      </p>
                      <p style={{ marginBottom: '5px' }}>
                        Luego de la fecha de vencimiento se cobrará un recargo
                        por mora mensual.
                      </p>
                    </div>
                  </td>
                </tr>
              </table>

              <table border={0} width={900}>
                <thead>
                  <td
                    style={{ textAlign: 'center', border: '2px solid black' }}
                  >
                    <h5 style={{ margin: 2 }}>DESCRIPCIÓN</h5>
                  </td>
                  <td
                    style={{ textAlign: 'center', border: '2px solid black' }}
                  >
                    <h5 style={{ padding: '0 10px', margin: 2 }}>IMPORTE</h5>
                  </td>
                </thead>
                {/* <tr>
                    <td colSpan={2}>
                      <p style={{marginBottom:'5px'}}>
                        Por concepto de mantenimiento de bienes, servicios y áreas comunes en beneficio del estacionamiento
                      </p>
                      <p style={{marginBottom:'5px'}}>{i ? 'CUOTA MENSUAL' : 'MANTENIMIENTO DE ÁREAS COMUNES'}</p>
                    </td>
                    <td colSpan={2}>
                      <h3>{i ? 'S/. 230.00' : 'S/. 140.97'}</h3>
                    </td>

                  </tr> */}
                {// i ?
                cochera.park.map(p => (
                  <tr style={{ fontSize: '14px' }}>
                    <td style={{ border: '2px solid black' }}>
                      <p
                        style={{ margin: 2, textTransform: 'uppercase' }}
                      >{`Por concepto de mantenimiento de servicios y áreas comunes en beneficio del estacionamiento ${p}`}</p>
                    </td>
                    <td
                      style={{ textAlign: 'center', border: '2px solid black' }}
                    >
                      {/* <p style={{marginBottom:'5px'}}>{i ? 'S/. 50.00' : 'S/. 30.65'}</p> */}
                      <p style={{ margin: 2 }}>30.65</p>
                    </td>
                  </tr>
                ))
                // :
                // <tr>
                //   <td colSpan={2}>
                //     <h3>CUOTA MANTENIMIENTO POR 19 DÍAS</h3>
                //   </td>
                //   <td colSpan={2}>
                //     <h3>S/. 140.97</h3>
                //   </td>
                // </tr>
                }
                <tr>
                  <td style={{ textAlign: 'right', paddingRight: '20px' }}>
                    <b>TOTAL S/. </b>
                  </td>
                  <td
                    style={{ textAlign: 'center', border: '2px solid black' }}
                  >
                    <p style={{ margin: 2 }}>61.30</p>
                  </td>
                </tr>
              </table>
              <div style={{ width: '900px', textAlign: 'center' }}>
                <h5 style={{ letterSpacing: '2px', margin: 0 }}>CANCELADO</h5>
                <p>Lince, ___________ de ________________ de 20___</p>
                <p style={{ marginBottom: 0, paddingTop: '10px' }} __>
                  ____________________________________________________
                </p>
                <p>p. Junta de Propietarios Edificio Saguaro</p>
              </div>
              {/* <table border={0} width={900}>
                  <tr>
                    <td>
                      <table border={0} width={500}>
                        <tr>
                          <td colSpan={2}>
                            <h5>CUENTA DE AHORROS EDIFICIO SAGUARO BCP</h5>
                          </td>
                        </tr>
                        <tr>
                          <td>NRO DE CUENTA DE AHORROS</td>
                          <td>
                            <b>193-93752936-0-33</b>
                          </td>
                        </tr>
                        <tr>
                          <td>NRO DE CUENTA INTERBANCARIA</td>
                          <td>
                            <b>002-193-193752936033-13</b>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>VASQUEZ DEL AGUILA REINEL</b>
                          </td>
                          <td>
                            <b>08440966</b>
                          </td>
                        </tr>
                        <tr>
                          <td>ENVIAR DEPOSITOS BANCO AL CORREO:</td>
                          <td>
                            <b>EDIFICIO.SAGUARO@GMAIL.COM</b>
                          </td>
                        </tr>
                        <tr>
                          <td colSpan={2}>
                            <b>(MINUSCULAS) INDICANDO EL NRO DEL DEPARTAMENTO</b>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td>
                      <div
                        style={{
                          textAlign: 'center',
                          border: '1px solid gray',
                          borderRadius: '5px',
                          padding: '5px',
                          margin: '5px'
                        }}
                      >
                        <p>Exonerado del pago de impuesto según D.L. 19620</p>
                        <p>
                          Según acuerdo de la Junta de Propietarios, los recibos
                          deben pagarse dentro de los 10 primeros días del mes
                          correspondiente.
                    </p>
                        <p>
                          Luego de la fecha de vencimiento se cobrará un recargo por
                          mora mensual.
                    </p>
                      </div>
                    </td>
                  </tr>
                </table> */}
            </div>
          ))
        )}
      </center>
    )
  }
}
