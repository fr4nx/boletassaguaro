import React, { Component } from 'react'
import { Usuarios } from '../csv/usuarios'
// import { Cocheras } from '../csv/cocheras'

// const boleta = (mes) => (

// )

export default class TestComponent extends Component {
  render () {
    const { usuarios } = Usuarios
    const fechaVencimiento = '20/01/2020'
    const fechaHoy = '02 de Enero de 2020'
    const mes = 'ENERO'
    const ultimoBoleta = 543 // 317 ultimo en setiembre
    return (
      <center>
        {usuarios.map((usuario, i) => (
          <div
            style={{
              // marginBottom: '50px',
              marginBottom: i++ % 2 ? '30px' : '110px',
              display: 'block'
            }}
            key={i}
          >
            <table border={0} width={900}>
              <tr>
                <td colSpan={2}>
                  <table border={0} width={600}>
                    <tr>
                      <td colSpan={2}>
                        <h3 style={{ margin: 5 }}>JUNTA DE PROPIETARIOS</h3>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h3 style={{ margin: 5 }}>EDIFICIO:</h3>
                      </td>
                      <td>
                        <h3 style={{ margin: 5 }}>SAGUARO</h3>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h4 style={{ margin: 5 }}>DIRECCIÓN:</h4>
                      </td>
                      <td>
                        <h4 style={{ margin: 5 }}>
                          AV. PARDO DE ZELA 570 - LINCE
                        </h4>
                      </td>
                    </tr>
                  </table>
                </td>
                <td colSpan={1}>
                  <table
                    width={300}
                    style={{ border: '1px solid gray', borderRadius: '5px' }}
                  >
                    <tr>
                      <td style={{ textAlign: 'center' }}>
                        Recibo de Mantenimiento de Áreas Comunes
                      </td>
                    </tr>
                    <tr>
                      <td style={{ textAlign: 'center' }}>
                        <b>Nro. {('000' + (i + ultimoBoleta)).slice(-4)}</b>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td />
                <td />
                <td style={{ textAlign: 'center' }}>
                  <h2>{mes} 2020</h2>
                </td>
              </tr>

              <tr>
                <td colSpan={3}>
                  <h3>Lima, {fechaHoy}</h3>
                </td>
              </tr>
            </table>
            <table border={0} width={900}>
              <tr>
                <td>Recibimos de:</td>
                <td colSpan={3} style={{ textTransform: 'uppercase' }}>
                  <b>{usuario.nombre}</b>
                </td>
              </tr>
              <tr>
                <td>La cantidad de:</td>
                <td colSpan={3}>DOSCIENTOS TREINTA Y 00/100 SOLES</td>
              </tr>
              <tr>
                <td>Del mes de:</td>
                <td>{mes}</td>
                <td>Año</td>
                <td>2020</td>
              </tr>
              <tr>
                <td>Departamento</td>
                <td>{usuario.dpto}</td>
                <td>
                  <b>Fecha de vencimiento</b>
                </td>
                <td>
                  <b>{fechaVencimiento}</b>
                </td>
              </tr>
              <br />
              <tr>
                <td colSpan={2}>
                  <h2>CUOTA MENSUAL</h2>
                </td>
                <td colSpan={2}>
                  <h2>S/. 230.00</h2>
                </td>
              </tr>
              {/* <tr>
                <td colSpan={2}>
                  <h2>CUOTA EXTRAORDINARIA</h2>
                </td>
                <td colSpan={2}>
                  <h2>S/. 250.00</h2>
                </td>
              </tr> */}
              <br />
              <tr>
                <td colSpan={4}>
                  CANCELADO, ...................... DE ...................... DE
                  ......................
                </td>
              </tr>
            </table>
            <table border={0} width={900}>
              <tr>
                <td>
                  <table border={0} width={500}>
                    <tr>
                      <td colSpan={2}>
                        <h5>CUENTA DE AHORROS EDIFICIO SAGUARO BCP</h5>
                      </td>
                    </tr>
                    <tr>
                      <td>NRO DE CUENTA DE AHORROS</td>
                      <td>
                        <b>193-93752936-0-33</b>
                      </td>
                    </tr>
                    <tr>
                      <td>NRO DE CUENTA INTERBANCARIA</td>
                      <td>
                        <b>002-193-193752936033-13</b>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>VASQUEZ DEL AGUILA REINEL</b>
                      </td>
                      <td>
                        <b>08440966</b>
                      </td>
                    </tr>
                    <tr>
                      <td>ENVIAR DEPOSITOS BANCO AL CORREO:</td>
                      <td>
                        <b>EDIFICIO.SAGUARO@GMAIL.COM</b>
                      </td>
                    </tr>
                    <tr>
                      <td colSpan={2}>
                        <b>(MINUSCULAS) INDICANDO EL NRO DEL DEPARTAMENTO</b>
                      </td>
                    </tr>
                  </table>
                </td>
                <td>
                  <div
                    style={{
                      textAlign: 'center',
                      border: '1px solid gray',
                      borderRadius: '5px',
                      padding: '5px',
                      margin: '5px'
                    }}
                  >
                    <p>Exonerado del pago de impuesto segun D.L. 19620</p>
                    <p>
                      Según acuerdo de la Junta de Propietarios, los recibos
                      deben pagarse dentro de los 10 primeros días del mes
                      correspondiente.
                    </p>
                    <p>
                      Luego de la fecha de vencimiento se cobrará un recargo por
                      mora mensual.
                    </p>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        ))}
      </center>
    )
  }
}
